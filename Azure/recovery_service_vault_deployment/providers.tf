variable management_subscription_id {
   type = string
   default = ""
}

terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>2.98.0"
    }
  }
}

provider "azurerm" {
  alias           = "mgnt"
  subscription_id = var.management_subscription_id
  features {}
}

# Configure the Microsoft Azure Provider
provider "azurerm" {
  subscription_id = ""
  tenant_id       = ""
  client_id       = ""
  client_secret   = ""
  features {}
}
