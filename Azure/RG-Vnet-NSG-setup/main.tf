# 1/09/2023
# Base Subscription Build

# Define Tags

variable tags {
    default     = {
	ClientCode		= "",
	ComplianceRequirements	= ""
	contact			= "",
	environment		= "",
	ProjectName		= "",
	ProjectVersion		= ""

	}	
    description = "List of tag name / tag value pairs"
    type        = map(string)
}

# Local Variables
locals {
	networkRG			= ""
	location			= ""
	appRG				= ""
	nsgapp				= ""
	nsgdb				= ""
	nsgaks				= ""
	nsgappgw			= ""
	vnet_name			= ""
	gwsubnetname			= ""
	appgwsubnetname			= ""
	privlinksubnetname		= ""
	apptiersubnetname		= ""
	dbtiersubnetname		= ""
	aksmgmttiersubnetname		= ""
	aksapptiersubnetname 		= ""
	vnetaddressspace		= ""
        vnettwoaddressspace		= ""
	dnssvr1				= ""
	dnssvr2				= ""
	gwsubnetcidr			= ""
	appgwsubnetcidr			= ""
	privlinksubnetcidr		= ""
	apptiersubnetcidr		= ""
	dbtiersubnetcidr		= ""
	aksmgmttiersubnetcidr 		= ""
        aksapptiersubnetcidr 		= ""
	diagstoreacctname		= ""
        postgresstoreacctname 	 	= ""
        aksstoreacctname		= ""
	diagstoracctkind		= ""
	diagstoraccttier		= ""
	diagstoracctrepl		= ""
	gateway_public_IP		= ""
	nat_gateway			= ""	
}
 
#Call Modules	

#builds Resource group

module "resgroup1" {
	source 		= "../../shared_modules/azure_network_resource_group"
	name 		= local.networkRG
	location 	= local.location
	tags 		= var.tags
}

module "resgroup2" {
	source 		= "../../shared_modules/azure_app_resource_group"
	name 		= local.appRG
	location 	= local.location
	tags 		= var.tags
}

#Builds the Network Security Groups

module "netsecgroup1" {
	source 				= "../../shared_modules/azure_network_SG"
	name 				= local.nsgapp
	location 			= module.resgroup1.location
	resource_group_name 		= module.resgroup1.name
	tags 				= var.tags
}

module "netsecgroup2" {
	source 				= "../../shared_modules/azure_network_SG"
	name 				= local.nsgdb
	location 			= module.resgroup1.location
	resource_group_name 		= module.resgroup1.name
	tags 				= var.tags		
}

module "netsecgroup3" {
	source 				= "../../shared_modules/azure_network_SG"
	name 				= local.nsgaks
	location 			= module.resgroup1.location
	resource_group_name 		= module.resgroup1.name
	tags 				= var.tags	
}

module "netsecgroup4" {
	source 				= "../../shared_modules/azure_network_SG"
	name 				= local.nsgappgw
	location 			= module.resgroup1.location
	resource_group_name 		= module.resgroup1.name
	tags 				= var.tags	
}

# Builds Network VNET Resources
# Name, Dns_Servers, security_group, address_prefix, service_endpoints are a list/array variable.  Reference to these object is in form of var.name[n] (n is the position of the variable starting at position 0)

module "network-vnet" {
	source 	 	    = "../../shared_modules/azure_vnet"	
	vnet_name	    = local.vnet_name
        sub_name            = [local.gwsubnetname, local.appgwsubnetname, local.privlinksubnetname, local.apptiersubnetname, local.dbtiersubnetname, local.aksmgmttiersubnetname, local.aksapptiersubnetname]
	location            = module.resgroup1.location
	resource_group_name = module.resgroup1.name
	address_space       = [local.vnetaddressspace, local.vnettwoaddressspace]
	dns_servers         = [local.dnssvr1, local.dnssvr2]
	security_group      = [module.netsecgroup1.id, module.netsecgroup2.id, module.netsecgroup3.id, module.netsecgroup4.id]
	address_prefix	    = [local.gwsubnetcidr, local.appgwsubnetcidr, local.privlinksubnetcidr, local.apptiersubnetcidr, local.dbtiersubnetcidr, local.aksmgmttiersubnetcidr, local.aksapptiersubnetcidr]
#	gwsubnetcidr	    = local.gwsubnetcidr
	service_endpoints   = ["Microsoft.EventHub"]
	tags 		    = var.tags
	depends_on 	    = [module.resgroup1]
}

/*
module "associate_network" {
    source               = "../../shared_modules/azure_associate_vnet_nsg_sg"
    security_group_name  = [local.nsgapp, local.nsgdb, local.nsgaks, local.nsgappgw]     
}
*/


#Builds Storage Account

module "storageaccts1" {
	source 				= "../../shared_modules/azure_storage_account"	
	name                		= local.diagstoreacctname
	location       			= module.resgroup2.location
	resource_group_name 		= module.resgroup2.name
	account_kind			= local.diagstoracctkind
	account_tier			= local.diagstoraccttier
	account_replication_type 	= local.diagstoracctrepl
	enable_https_traffic_only	= "true"
	tags 				= var.tags	

}

module "storageaccts2" {
	source 				= "../../shared_modules/azure_storage_account"	
	name                		= local.postgresstoreacctname
	location       			= module.resgroup2.location
	resource_group_name 		= module.resgroup2.name
	account_kind			= local.diagstoracctkind
	account_tier			= local.diagstoraccttier
	account_replication_type 	= local.diagstoracctrepl
	enable_https_traffic_only	= "true"
	tags 				= var.tags	

}	

module "storageaccts3" {
	source 				= "../../shared_modules/azure_storage_account"	
	name                		= local.aksstoreacctname
	location       			= module.resgroup2.location
	resource_group_name 		= module.resgroup2.name
	account_kind			= local.diagstoracctkind
	account_tier			= local.diagstoraccttier
	account_replication_type 	= local.diagstoracctrepl
	enable_https_traffic_only	= "true"
	tags 				= var.tags	
}

module "gateway_publicIP" {
        source			= "../../shared_modules/azure_nat_gateway"
	gateway_ip_name		= local.gateway_public_IP
	location		= local.location
	resource_group_name	= local.networkRG
	gateway_name		= local.nat_gateway
        depends_on = [module.network-vnet]
}
