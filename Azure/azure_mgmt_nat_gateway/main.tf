# Local Variables
locals {
        networkRG                       = ""
        location                        = ""
        gateway_public_IP               = ""
        nat_gateway                     = ""
}


module "gateway_publicIP" {
        source                  = "../../shared_modules/azure_nat_gateway"
        gateway_ip_name         = local.gateway_public_IP
        location                = local.location
        resource_group_name     = local.networkRG
        gateway_name            = local.nat_gateway
}
