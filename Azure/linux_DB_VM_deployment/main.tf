module "server" {

   source = "../../shared_modules/linux_server_template"
   resourceGroupName = ""
   location = ""
   vmCount = 1
   vmName = ""
   availabilitySet = ""
   vmSize = "Standard_D8s_v4"
   addressOffset = 1
   subnet_name = ""
   vnet_name = ""
   network_rg_name = ""
}
