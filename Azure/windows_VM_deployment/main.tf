module "windows_server" {

   source = "../../shared_modules/windows_server_template"
   resourceGroupName = ""
   location = ""
   vmCount = 1
   vmName = ""
   availabilitySet = ""
   vmSize = "Standard_D8s_v4"
   subnet_name = "" 
   addressOffset = 1
   vnet_name = ""
   network_rg_name = ""
}
