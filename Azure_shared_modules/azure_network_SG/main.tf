resource "azurerm_network_security_group" "Netsecgroup" {
  name                = var.name
  location            = var.location
  resource_group_name = var.resource_group_name

 # security_rule {
 #   name                       = "test123"
 #   priority                   = 100
 #   direction                  = "Inbound"
 #   access                     = "Allow"
 #   protocol                   = "Tcp"
 #   source_port_range          = "*"
 #   destination_port_range     = "*"
 #   source_address_prefix      = "*"
 #   destination_address_prefix = "*"
 # }

  tags = var.tags
}

#output Variables

output "id" {
    value = azurerm_network_security_group.Netsecgroup.id
	description = "Data Resource Group ID value "
}

output "name" {
    value = azurerm_network_security_group.Netsecgroup.name
	description = "Data Resource Group Name value "
}