#Define Variables

variable name {
    description = "Network Security Group Name"
    type        = string
}

variable location {
    description = "Network Security Group location"
    type        = string
}

variable tags {
    default     = {
        Automation = "Terraform",
        Department = ""
    }
    description = "List of tag name / tag value pairs"
    type        = map(string)
}

variable resource_group_name {
    description = "Resource Group Name"
    type        = string
}
