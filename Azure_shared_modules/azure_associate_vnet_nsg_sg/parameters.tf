variable resource_group_name {
    description = "Resource Group Name"
    type        = string
}

variable location {
    description = "Virtual Network location"
    type        = string
}

variable address_space {
    description = "Network Address Space"
    type        = list(string)
}

variable sub_name {
    description = "Subnet Names"
    type        = list(string)
}

variable vnet_name {
    description = "Virtual Network Name"
    type        = string
}

variable security_group_name { 
   description = "Names of Network Security Groups"
   type = list(string)
}