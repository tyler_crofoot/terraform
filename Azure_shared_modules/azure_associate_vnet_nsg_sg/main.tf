resource "azurerm_subnet_network_security_group_association" "nsg_assoc" {
   count = var.security_group_name == "none" ? 0 : 1
   subnet_id = azurerm_subnet.subnet.id
   network_security_group_id = azurerm_network_security_group.Network-RG.*.id[0]
}

/*
output "subnet" {
   value = map("name",azurerm_subnet.subnet.name,
                  "subnet_id", azurerm_subnet.subnet.id,
                  "cidr_block", azurerm_subnet.subnet.address_prefix)
}
*/