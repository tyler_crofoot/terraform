
resource "azurerm_recovery_services_vault" "vault" {
  name                = var.rsv_name
  location            = var.location
  resource_group_name = var.resourceGroupName
  sku                 = "Standard"
  storage_mode_type   = "GeoRedundant"
  tags		      = var.common-tags
  
  soft_delete_enabled = true
}
