variable rsv_name {
    description = "Recovery Service Vault Name"
    type        = string
}

variable resourceGroupName {
    description = "Resource Group Name"
    type        = string
}

variable location {
    description = "Region"
    type        = string
}

variable common-tags {
    default     = {
	ClientCode		= "",
	contact			= "",
	environment		= "",
	ProjectName		= ""
	}	
    description = "List of tag name / tag value pairs"
    type        = map(string)
}
