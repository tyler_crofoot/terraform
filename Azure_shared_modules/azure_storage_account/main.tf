resource "azurerm_storage_account" "storageaccount" {
        name = var.name
        resource_group_name = var.resource_group_name
        location = var.location
        account_kind = var.account_kind
        account_tier = var.account_tier
        account_replication_type = var.account_replication_type
        enable_https_traffic_only = var.enable_https_traffic_only

   tags = var.tags
}


# Must have storage endpoint enabled on the subnet for next module to work

#resource "azurerm_storage_account_network_rules" "storageaccountnetwork" {
#  resource_group_name = azurerm_storage_account.resource_group_name
#  storage_account_name = var.name
# storage_account_id = azurerm_storage_account.storageaccount.id
#  default_action             = "Deny"
 # virtual_network_subnet_ids = [data.azurerm_subnet.appsubnet.id]
#  depends_on = [
#        azurerm_storage_account.storageaccount
 # ]
#}
