# Variables

variable name {
    description = "Storage Account Name"
    type        = string
}

variable resource_group_name {
    description = "Resource Group Name"
    type        = string
}

variable location {
    description = "Location Name"
    type        = string
}

variable account_kind {
    description = "Storage Account SKU"
    type        = string
}

variable account_tier {
    description = "Storage Account Tier"
    type        = string
}

variable account_replication_type {
    description = "Storage Account replication"
    type        = string
}

variable enable_https_traffic_only {
    description = "Storage Account HTTPS"
    type        = string
}

variable tags {
    default     = {
        Automation = "Terraform",
        Department = ""
    }
    description = "List of tag name / tag value pairs"
    type        = map(string)
}
