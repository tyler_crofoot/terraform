# Creates Network Resource Group

resource "azurerm_resource_group" "Network-RG" {
	name		= var.name
	location	= var.location
	tags            = var.tags

} 

#output Variables

output "id" {
    value = azurerm_resource_group.Network-RG.id
	description = "Network Resource Group ID value "
}

output "name" {
    value = azurerm_resource_group.Network-RG.name
	description = "Network Resource Group Name value "
}

output "location" {
    value = azurerm_resource_group.Network-RG.location
	description = "Network Resource Group location value "
}
