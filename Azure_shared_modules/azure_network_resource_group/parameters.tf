#Define Variables

variable name {
    description = "Resource Group Name"
    type        = string
}

variable location {
    description = "Resource Group location"
    type        = string
}

variable tags {
    default     = {
        Automation = "Terraform",
        Department = ""
    }
    description = "List of tag name / tag value pairs"
    type        = map(string)
}
