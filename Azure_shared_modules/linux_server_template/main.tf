# Catalogs the Subnet

data "azurerm_subnet" "db_subnet" {
        name                 = var.subnet_name
        virtual_network_name = var.vnet_name
        resource_group_name  = var.network_rg_name
}

resource "azurerm_availability_set" "availabilitySet" {
  name = var.availabilitySet
  location = var.location
  resource_group_name = var.resourceGroupName
  managed = "true"
  platform_fault_domain_count = "2"
  platform_update_domain_count = "5"
}

resource "azurerm_network_interface" "nic" {
   count = var.vmCount
   name = format("%s0%d-nic", var.vmName, count.index+1)
   resource_group_name = var.resourceGroupName
   location = var.location

   ip_configuration {
      name = format("%s0%d-nic-ip-config", var.vmName, count.index+1)
      subnet_id = data.azurerm_subnet.db_subnet.id
      private_ip_address_allocation = "static"
      private_ip_address = cidrhost(var.subnet["subnet_cidr"], count.index + var.cloud_reserved + var.addressOffset)
   }
}

#Create Managed Data Disk 01
resource "azurerm_managed_disk" "datadisk01" {
  count		       = var.vmCount
  name                 = format("%s0%d-datadisk01", var.vmName, count.index+1) 
  location             = var.location
  resource_group_name  = var.resourceGroupName
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "512"
  tags 		       = var.common-tags
}

resource "azurerm_virtual_machine" "virtualMachine" {
  count = var.vmCount
  name = format("%s0%d", var.vmName, count.index+1)
  resource_group_name = var.resourceGroupName
  location = var.location
  network_interface_ids = [ element(azurerm_network_interface.nic.*.id, count.index)]
  vm_size = var.vmSize
  availability_set_id = azurerm_availability_set.availabilitySet.id
  tags = var.common-tags
  delete_os_disk_on_termination = "true"
  delete_data_disks_on_termination = "true"

  storage_image_reference {
    publisher = var.rhelstorageImageReference["publisher"]
    offer     = var.rhelstorageImageReference["offer"]
    sku       = var.rhelstorageImageReference["sku"]
    version   = var.rhelstorageImageReference["version"]
  }

  storage_os_disk {
    name = format("%s0%d-os-disk", var.vmName, count.index+1)
    managed_disk_type = var.storageOsDisk["type"]
    create_option = var.storageOsDisk["createOption"]
    caching = var.storageOsDisk["caching"]
    disk_size_gb = "256"
  }


  os_profile {
    computer_name  = format("%s0%d", var.vmName, count.index+1)
    admin_username = var.osProfile["adminUsername"]
    admin_password = var.osProfile["adminPassword"]
  }

  os_profile_linux_config {
    disable_password_authentication = "false"
  }

  boot_diagnostics {
    enabled = "true"
   storage_uri = "https://ogdermsdiagsa.blob.core.windows.net/"
  }
}

resource "azurerm_virtual_machine_data_disk_attachment" "datadisk01" {
    count = var.vmCount
    managed_disk_id    = azurerm_managed_disk.datadisk01.*.id[count.index]
    virtual_machine_id = azurerm_virtual_machine.virtualMachine.*.id[count.index]
    lun                = "0"
    caching            = "ReadOnly"
}
