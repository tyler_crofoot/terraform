variable cloud_reserved {
   type = string
   default = "4"
}

variable resourceGroupName {
   type = string
}

variable location {
   type = string
}

variable vmCount  {
   type = string
}

variable vmName {
   type = string
}

variable vmSize {
   type = string
}

variable rhelstorageImageReference {
   type = map
   default = {
      publisher = "RedHat"
      offer = "RHEL"
      sku = "8_5"
      version = "latest"
      os_type = "Linux"
   }
}

variable storageOsDisk {
   type = map
   default = {
      type = "Premium_LRS"
      createOption = "FromImage"
      caching = "ReadWrite"
   }
}

#variable storageAccount {
#   type = map
#}

variable addressOffset {
   type = string
   default = "0"
}

variable subnet_name {
   type = string
}

variable subnet {
   type = map 
   default = {
	"subnet_cidr"       = ""
  }
}

variable vnet_name {
   type = string
}

variable network_rg_name {
   type = string
}

variable osProfile {
   type = map
   default = {
      computerNamePrefix = ""
      adminUsername = ""
      adminPassword = ""
   }
}

variable lbBePools {
   description = "load balancer backend pool"
   type        = list
   default     = []
}

 variable availabilitySet {
    type = string
}

variable common-tags {
   type = map
   default = {
      project     = ""
      abbrev      = ""
      client      = ""
      environment = ""
      contact     = ""
   }
}

variable ppgID {
   description = "proximity placement groups"
   type        = string
   default     = "NONE"
}

/*
variable storage_URL {
    description = "URL for storage account used for boot diagnostics logs"
    type        = string
}
*/

variable dns_servers {
   type = list
   default = ["", ""]
}
