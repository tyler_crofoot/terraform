variable gateway_ip_name {
    description = "Nat Gateway IP Name"
    type        = string
}

variable gateway_name {
    description = "Gateway Name"
    type        = string
}

variable resource_group_name {
    description = "Resource Group Name"
    type        = string
}

variable location {
    description = "Virtual Network location"
    type        = string
}

variable vnet_name {
    description = "Virtual Network Name"
    type        = string
    default	= ""
}

variable sub_name {
    description = "Subnet Name"
    type        = map
    default     = {
	"sub-0" = ""
   }
}
