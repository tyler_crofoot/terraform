
data "azurerm_subnet" "ogderms-nprd-aks" {
  name                 = var.sub_name["sub-0"]
  virtual_network_name = var.vnet_name
  resource_group_name  = var.resource_group_name
}

resource "azurerm_public_ip" "publicIP" {
  name                = var.gateway_ip_name
  location            = var.location
  resource_group_name = var.resource_group_name
  allocation_method   = "Static"
  sku                 = "Standard"
}

resource "azurerm_nat_gateway" "ogderms_gateway" {
  name                    = var.gateway_name
  location                = var.location
  resource_group_name     = var.resource_group_name
#  public_ip_address_ids   = [azurerm_public_ip.publicIP.id]
  sku_name                = "Standard"
  idle_timeout_in_minutes = 10
}

resource "azurerm_subnet_nat_gateway_association" "nat_sub_associate" {
  subnet_id      = data.azurerm_subnet.ogderms-nprd-aks.id
  nat_gateway_id = azurerm_nat_gateway.ogderms_gateway.id
}

output "nat_gateway" {
    value = azurerm_nat_gateway.ogderms_gateway.id
        description = "Resource ID of the Nat Gateway"
}
