resource "azurerm_resource_group" "Application-RG" {
	name		= var.name
	location	= var.location
	tags 		= var.tags

} 

#output Variables

output "id" {
    value = azurerm_resource_group.Application-RG.id
	description = "Application Resource Group ID value "
}

output "name" {
    value = azurerm_resource_group.Application-RG.name
	description = "Application Resource Group Name value "
}

output "location" {
    value = azurerm_resource_group.Application-RG.location
	description = "Application Resource Group location value "
}