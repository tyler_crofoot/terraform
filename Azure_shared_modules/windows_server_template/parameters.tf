variable cloud_reserved {
   type = string
   default = "4"
}

variable resourceGroupName {
   type = string
}

variable location {
   type = string
}

variable vmCount  {
   type = string
}

variable vmName {
   type = string
}

variable vmSize {
   type = string
}

/*
variable storageAccount {
   type = map
}
*/

variable addressOffset {
   type = string
   default = 0
}

variable subnet_name {
  type = string
}

variable subnet {
   type = map
   default = {
     "subnet_cidr" = ""
  }
}

variable storageImageReference {
   type = map
   default = {
     "publisher" = "MicrosoftWindowsServer"
     "offer" = "WindowsServer"
     "sku" = "2019-Datacenter"
     "version" = "latest"
  }
}

variable storageOsDisk {
   type = map
   default = {
     "type" = "Premium_LRS"
     "createOption" = "FromImage"
     "caching" = "ReadWrite"
  }
}

/*
variable storageDataDisk {
   type = map
}
*/

variable osProfile {
   type = map
   default = {
     adminUsername = ""
     adminPassword = ""
   }
}

/*
variable lbBePools {
   type = list
   default = []
}
*/

variable availabilitySet {
    type = string
}

variable tags {
   type = map
   default = {
      project     = ""
      abbrev      = ""
      client      = ""
      environment = ""
      contact     = ""
   }
}

/*
variable ppgID {
    type = string
}

variable accNetwork {
    type = string
}

variable storage_URL {
    description = "URL for storage account used for boot diagnostics logs"
    type        = string
}
*/

variable vnet_name {
  type = string
}

variable network_rg_name {
  type = string
}

variable dns_servers {
   type = list
   default = ["", ""]
}
