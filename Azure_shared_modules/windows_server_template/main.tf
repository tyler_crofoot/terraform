
data "azurerm_subnet" "_subnet" {
        name                 = var.subnet_name
        virtual_network_name = var.vnet_name
        resource_group_name  = var.network_rg_name
}

resource "azurerm_availability_set" "availabilitySet" {
# count = var.availabilitySet["enabled"] == true ? 1 : 0
  name = var.availabilitySet
  location = var.location
  resource_group_name = var.resourceGroupName
  managed = "true"
  platform_fault_domain_count = "2"
  platform_update_domain_count = "5"
}



resource "azurerm_network_interface" "nic" {
   count = var.vmCount
   name = format("%s0%d-nic", var.vmName, count.index)
   resource_group_name = var.resourceGroupName
   location = var.location

   ip_configuration {
      name = format("%s0%d-nic-ip-config", var.vmName, count.index)
      subnet_id = data.azurerm_subnet._subnet.id 
      private_ip_address_allocation = "static"
      private_ip_address = cidrhost(var.subnet["subnet_cidr"], count.index + var.cloud_reserved + var.addressOffset)
   }
}

resource "azurerm_virtual_machine" "virtualMachine" {
  count = var.vmCount
  name = format("%s0%d", var.vmName, count.index)
  resource_group_name = var.resourceGroupName
  location = var.location
  network_interface_ids = [element(azurerm_network_interface.nic.*.id, count.index)]
  vm_size = var.vmSize
  availability_set_id = azurerm_availability_set.availabilitySet.id

  storage_image_reference {
    publisher = var.storageImageReference["publisher"]
    offer     = var.storageImageReference["offer"]
    sku       = var.storageImageReference["sku"]
    version   = var.storageImageReference["version"]
  }

  storage_os_disk {
    name = format("%s0%d-os-disk", var.vmName, count.index)
    managed_disk_type = var.storageOsDisk["type"]
    create_option = var.storageOsDisk["createOption"]
    caching = var.storageOsDisk["caching"]
    disk_size_gb = "256"
  }

/*
  storage_data_disk {
    name = format("%s0%d-data-disk", var.vmName, count.index) 
    #count = var.storageDataDisk["diskCount"]
    managed_disk_type = var.storageDataDisk["type"]
    create_option = var.storageDataDisk["createOption"]
    disk_size_gb = var.storageDataDisk["diskSizeGb"]
    caching = var.storageDataDisk["caching"]
    lun = "0"
  }
*/

  os_profile {
    computer_name  = format("%s0%d", var.vmName, count.index)
    admin_username = var.osProfile["adminUsername"]
    admin_password = var.osProfile["adminPassword"]
  }

  os_profile_windows_config {
   provision_vm_agent = "true"
   enable_automatic_upgrades = "false"

  }

  boot_diagnostics {
    enabled = "true"
    storage_uri = ""
  }


  delete_os_disk_on_termination = "true"
  delete_data_disks_on_termination = "true"

  tags = var.tags
}

/*  
  resource "azurerm_virtual_machine_extension" "networkWatcher" {
    count = var.vmCount
    name = format("%s0%d", var.vmName, count.index +1)-networkWatcher"
    virtual_machine_id = element(azurerm_virtual_machine.virtualMachine.*.id, count.index)
    publisher = "Microsoft.Azure.NetworkWatcher"
    type = "NetworkWatcherAgentWindows"
    type_handler_version = "1.4"
    auto_upgrade_minor_version = "true"
  }
*/
