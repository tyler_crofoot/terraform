#Define Variables

variable vnet_name {
    description = "Virtual Network Name"
    type        = string
}

variable location {
    description = "Virtual Network location"
    type        = string
}

variable tags {
    default     = {
        Automation = "Terraform",
        Department = ""
    }
    description = "List of tag name / tag value pairs"
    type        = map(string)
}

variable resource_group_name {
    description = "Resource Group Name"
    type        = string
}

variable service_endpoints {
    description = "A list of Azure service endpoints"
    type        = list(string)
}

variable address_space {
    description = "Network Address Space"
    type        = list(string)
}

variable dns_servers {
    description = "DNS Servers"
    type        = list(string)
}

variable address_prefix {
    description = "Subnet Address Prefix"
    type        = list(string)
}

variable security_group {
    description = "Security Group ID"
    type        = list(string)
}

variable sub_name {
    description = "Subnet Names"
    type        = list(string)
}
